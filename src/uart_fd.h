#ifndef UART_FD_H
#define UART_FD_H

#include <simavr/fifo_declare.h>
#include <simavr/sim_avr.h>

DECLARE_FIFO(uint8_t, fd_fifo, 1024);

typedef struct uart_fd_t {
    avr_irq_t *irq;         // IRQ array
    struct avr_t *avr;      // AVR core

    pthread_t thread;       // IO handling thread
    volatile uint8_t xon;   // AVR input enable flag
    
    int in_fd;              // Input FD
    int out_fd;             // Output FD

    fd_fifo_t avr_out_fifo; // Output from the AVR
    fd_fifo_t avr_in_fifo;  // Input to the AVR
} uart_fd_t;

uart_fd_t * uart_fd_init(struct avr_t * avr, int in_fd, int out_fd, uint8_t uart_index);

// Needs to be called from the AVR thread in order to
// handle interrupt driven UART RX correctly.
void send_data_to_avr(uart_fd_t *info);

#endif // UART_FD_H
