#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "get_options.h"


#define print_usage() printf("Usage: %s -f <flash_file> [-e <eeprom_file>] [-g[debug_port]] [-p[uart_port]] [-v]\n", argv[0])

int get_options(int argc, char *argv[], struct sim_options * options)
{
    options->debug_port = -1;
    options->uart_port = -1;
    options->trace = 0;

    options->flash_hex = NULL;
    options->eeprom_hex = NULL;


    int opt;
    while ((opt = getopt(argc, argv, "f:e:g::p::v")) != -1)
    {
        switch(opt)
        {
            // Flash data
            case 'f':
                options->flash_hex = strdup(optarg);
                break;
            
            // EEPROM data
            case 'e':
                options->eeprom_hex = strdup(optarg);
                break;

            // Debug port
            case 'g':
                if (optarg)
                    options->debug_port = atoi(optarg);
                else
                    options->debug_port = 1234;

                if ((0 >= options->debug_port) || (options->debug_port > 65535))
                {
                    print_usage();
                    return -1;
                }
                break;
            
            // UART port
            case 'p':
                if (optarg)
                    options->uart_port = atoi(optarg);
                else
                    options->uart_port = 5555;

                if ((0 >= options->uart_port) || (options->uart_port > 65535))
                {
                    print_usage();
                    return -1;
                }
                break;

            // TRACE vrbosity
            case 'v':
                options->trace++;
                break;

            default:
                print_usage();
                return -1;
        }
    }

    if (options->flash_hex == NULL)
    {
        print_usage();
        return -1;
    }

    return 0;
}