#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netdb.h>

#define handle_error(msg) \
    { perror(msg); exit(EXIT_FAILURE); }

// This function binds to a port, accepts a single connection and returns the FD.
int get_client(int port)
{
    int ret;
    
    struct addrinfo hints;
    struct addrinfo *result, *rp;
    int sockfd, clientfd;
    
    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_UNSPEC;        // IPv4/6
    hints.ai_socktype = SOCK_STREAM;    // TCP
    hints.ai_flags = AI_PASSIVE | AI_NUMERICSERV; // Bind on wildcard addr, numeric port
    hints.ai_protocol = 0;
    hints.ai_canonname = NULL;
    hints.ai_addr = NULL;
    hints.ai_next = NULL;

    char port_str[6];
    snprintf(port_str, sizeof(port_str), "%d", port);

    // Get a list of addresses
    ret = getaddrinfo(NULL, port_str, &hints, &result);
    if (ret != 0)
    {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(ret));
        exit(EXIT_FAILURE);
    }
    
    // Go through the list until one binds
    for (rp = result; rp != NULL; rp = rp->ai_next)
    {
        sockfd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
        if (sockfd == -1)
            continue; // Socket failed to open. Try next address.
        
        ret = bind(sockfd, rp->ai_addr, rp->ai_addrlen);
        
        if (ret == 0)
            break; // Socket bound. Continue on.
        
        // Failed to bind. Try next address.
        close(sockfd);
    }
    
    if (rp == NULL)
    {
        // Failed to find an address
        fprintf(stderr, "Could not bind\n");
        exit(EXIT_FAILURE);
    }
    
    freeaddrinfo(result);
    
    ret = listen(sockfd, 1);
    if (ret != 0)
        handle_error("listen");
    
    clientfd = accept(sockfd, NULL, NULL);
    
    if (clientfd == -1)
        handle_error("accept");
    
    close(sockfd);
    
    return clientfd;
}
