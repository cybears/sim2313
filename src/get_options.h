#ifndef GET_OPTIONS_H
#define GET_OPTIONS_H

struct sim_options
{
    int uart_port;
    int debug_port;
    int trace;

    char * flash_hex;
    char * eeprom_hex;
};

int get_options(int argc, char *argv[], struct sim_options * options);

#endif // GET_OPTIONS_H