#define F_CPU 8000000
#define BAUD 9600

#include <avr/io.h>
#include <util/setbaud.h>

int main()
{
    // Init UART
    UCSRC |= (3 << UCSZ0); // 8 bits

    UBRRH = UBRRH_VALUE;
    UBRRL = UBRRL_VALUE;
#if USE_2X
    UCSRA |= (1 << U2X);
#else
    UCSRA &= ~(1 << U2X);
#endif
    UCSRB = (1 << RXEN) | (1 << TXEN);

    // Echo uart
    while(1)
    {
        char c;
        loop_until_bit_is_set(UCSRA, RXC);
        c = UDR;

        loop_until_bit_is_set(UCSRA, UDRE);
        UDR = c;
    }
}