FROM gcc

# Requirements to build the simulator and FW
RUN apt update && apt install -y avr-libc libsimavr-dev gcc-avr gdb-avr

RUN mkdir -p /sim/src

WORKDIR /sim

COPY src src
COPY test_fw test_fw

# Build the simulator
RUN cd src/ && make
RUN cp src/sim_main ./

# Build the test firmware
RUN cd test_fw/ && make
RUN cp test_fw/test.hex ./

# Run the test FW. Extra verbose.
CMD ./sim_main -f test.hex -vvvv
