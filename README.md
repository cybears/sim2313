# ATtiny2313 Simulator

This is a simulator of the ATtiny2313 that makes use of the _simavr_ library (https://github.com/buserror/simavr).

## Docker Image
A pre-built docker image is available:  **registry.gitlab.com/cybears/sim2313**

By default this image will run the simulator with a test firmware that echoes any UART input. To run it use commands like these:
```bash
# Run the default firmware
docker run -i -t  registry.gitlab.com/cybears/sim2313

# Interactive terminal
docker run -i -t  registry.gitlab.com/cybears/sim2313 bash
```

The docker image contains a working simulator and requirements to debug and build firmware binaries.

## Debugging firmware
The simulator can act as a GDB server in order to debug firmware.
### Simulator
```bash
> ./sim_main -f test.hex -g1234
avr_gdb_init listening on port 1234
```
### Debugger
```bash
> avr-gdb test_fw/test.elf
...
Reading symbols from test_fw/test.elf...done.
(gdb) target remote :1234
Remote debugging using :1234
```

## Notes
This repository and its artifacts are supplementary material. There are no flags here.
